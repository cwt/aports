# Contributor: Daiki Maekawa <daikimaekawa29@gmail.com>
# Maintainer: Daiki Maekawa <daikimaekawa29@gmail.com>
pkgname=py3-frozendict
_pkgname=frozendict
pkgver=2.3.9
pkgrel=0
pkgdesc="immutable dictionary"
url="https://github.com/Marco-Sulla/python-frozendict"
arch="all"
license="LGPL-3.0-or-later"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-frozendict" # Backwards compatibility
provides="py-frozendict=$pkgver-r$pkgrel" # Backwards compatibility

# C extension lacks py311 support
export FROZENDICT_PURE_PY=1

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
c0f589e71a9bed8bfae52ffd8b4c28e5f0cabd64d96d9597cd4b88961660e4c2a907e1011145acdc0770c7f0fc4c7568b9cc95f999b00ff018e22e14a2895d62  frozendict-2.3.9.tar.gz
"
